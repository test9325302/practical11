$(document).ready(function() {
    // Отправка GET-запроса к API для получения списка постов
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts',
        method: 'GET',
        success: function(posts) {
            // Обработка успешного ответа от сервера
            displayPosts(posts);
        },
        error: function() {
            // Обработка ошибки при получении данных
            alert('Ошибка при загрузке постов!');
        }
    });
 
    // Функция для отображения списка постов на странице
    function displayPosts(posts) {
        var $postsContainer = $('#posts-container');
        // Очистка контейнера перед добавлением новых постов
        $postsContainer.empty();
        // Добавление каждого поста в контейнер
        posts.forEach(function(post) {
            var $post_col = $( '<div class="col-sm-6 mb-3 mb-sm-4"></div>');
            var $post_card = $('<div class="card h-100">');
            var $post = $('<div class="card-body ">');
            $post.append('<h2 class="card-title">' + post.title + '</h2>');
            $post.append('<p class="card-text">' + post.body + '</p>');
            $post.append('<p class="card-text">' + post.id + '</p>');
            $post.append('<button class="btn btn-primary post-edit" data-post-id="' + post.id + '">Edit</button>');    
            $post.append('<button class="delete-post btn btn-danger" data-post-id="' + post.id + '">Delete</button>');
            $post_card.append($post)
            $post_col.append($post_card)
            $postsContainer.append($post_col);
        });
    // Добавляем слушатели событий для кнопок удаления
    $('.delete-post').click(function() {
        var postId = $(this).data('post-id');
        var confirmDelete = confirm('Вы уверены, что хотите удалить этот пост?');
        if (confirmDelete) {
            deletePost(postId);
            console.log(postId);
        }
    }); 
// Функция для удаления поста
function deletePost(postId) {
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts/' + postId,
        method: 'DELETE',
        success: function() {
            // После успешного удаления поста обновляем список постов
            loadPosts(); // Загружаем посты снова
        },
        error: function() {
            // Обработка ошибок при удалении поста
            alert('Ошибка при удалении поста!');
        }
    });
}  
// Функция для загрузки списка постов
function loadPosts() {
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts',
        method: 'GET',
        success: function(posts) {
            // Обработка успешного ответа от сервера
            displayPosts(posts);
        },
        error: function() {
            // Обработка ошибки при получении данных
            alert('Ошибка при загрузке постов!');
        }
    });
}
    }

});

$(document).ready(function() {
    $('#post_list').click(function() {
        $('#posts-container').css('display', 'flex');
    });
});

$(document).ready(function() {
    // Обработчик нажатия на кнопку "Добавление поста"
    $('#post_add').click(function() {
        // Открытие модальной формы для создания нового поста
        $('#createPostModal').modal('show');
    });

    // Обработчик отправки формы создания нового поста
    $('#createPostForm').submit(function(event) {
        // Предотвращаем стандартное поведение формы
        event.preventDefault();

        // Получение данных из формы
        var postData = {
            title: $('#postTitle').val(),
            body: $('#postBody').val(),
            userId: 1 // ID пользователя, который создает пост (может быть изменено в зависимости от вашей логики)
        };

        // Отправка запроса POST к API для создания нового поста
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts',
            method: 'POST',
            data: JSON.stringify(postData),
            contentType: 'application/json',
            success: function(response) {
                // Обработка успешного создания поста
                alert('Пост успешно создан!');
                // Закрытие модальной формы
                $('#createPostModal').modal('hide');
                    // Создание HTML-элемента для нового поста
    var newPost = $('<div class="col-sm-6 mb-3 mb-sm-4"></div>');
    newPost.append('<div class="card h-100"><div class="card-body"><h2 class="card-title">' 
    + response.title + '</h2><p class="card-text">'
    + response.body +'<p class="card-text">' + response.id + '</p>'+ '<button class="btn btn-primary post-edit" data-post-id="' + response.id + '">Edit</button> <button class="delete-post btn btn-danger" data-post-id="' + response.id + '">Delete</button></div></div>');

    // Добавление нового поста в начало списка
    $('#posts-container').prepend(newPost);
            },
            error: function() {
                // Обработка ошибки при создании поста
                alert('Ошибка при создании поста!');
            }
        });
    });
});


// Обработчик события клика на кнопку "Edit" в каждом посте
$(document).on('click', '.post-edit', function() {
    // Получаем ID поста
    var postId = $(this).data('post-id');
    console.log(postId)
    // Отправляем запрос GET к API для получения данных о посте
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts/' + postId,
        method: 'GET',
        success: function(post) {
            // Заполняем форму редактирования данными текущего поста
            $('#editPostModal input[name="title"]').val(post.title);
            $('#editPostModal textarea[name="body"]').val(post.body);
            // Устанавливаем атрибут data-post-id для кнопки сохранения изменений
            $('#editPostModal button[type="submit"]').attr('data-post-id', postId);
            // Открываем модальное окно редактирования поста
            $('#editPostModal').modal('show');
        },
        error: function() {
            // Обработка ошибки при получении данных о посте
            alert('Ошибка при загрузке данных о посте!');
        }
    });
});

// Обработчик события отправки формы редактирования поста
$('#editPostForm').submit(function(event) {
    event.preventDefault();
    // Получаем данные формы редактирования
    var postData = {
        title: $(this).find('input[name="title"]').val(),
        body: $(this).find('textarea[name="body"]').val(),
        userId: 1 // Ваш ID пользователя или другой способ получения ID
    };
    var postId = $(this).find('button[type="submit"]').data('post-id');

    // Отправляем запрос PUT к API для редактирования поста
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts/' + postId,
        method: 'PUT',
        data: JSON.stringify(postData),
        contentType: 'application/json; charset=UTF-8',
        success: function() {
            // Обработка успешного редактирования поста
            alert('Пост успешно отредактирован!');
            // Закрываем модальное окно редактирования поста
            $('#editPostModal').modal('hide');
            // Обновляем значения в существующем посте
            $('#post-' + postId + ' .post-title').text(postData.title);
            
            $('#post-' + postId + ' .post-body').text(postData.body);
        },
        error: function() {
            // Обработка ошибки при редактировании поста
            alert('Ошибка при редактировании поста!');
        }
    });
});

